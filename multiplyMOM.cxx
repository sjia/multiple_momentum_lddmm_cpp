#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main( int argc, char* argv[] )
{
  if( argc != 4 )
    {
    std::cerr << "Usage: "<< std::endl;
    std::cerr << argv[0];
    std::cerr << " <inputfile> <multiplybym> <outputfile>";
    std::cerr << std::endl;
    return EXIT_FAILURE;
    }

    const char * inputFileName   = argv[1];
    int m;
    m=std::stoi(argv[2]);
    const char * outputFileName  = argv[3];

    float x,y,z;

    ifstream infile(inputFileName);
    ofstream outfile(outputFileName);
    if (infile.is_open())
    {
        while ( infile >> x >> y >>z )
        {
            cout << x << " " << y << " " << z << endl;
            x = x*m;
            y = y*m;
            z = z*m;
	    outfile << x << " " << y << " " << z << endl;
        }
        infile.close();
        outfile.close( );
    }
    else
    {
        cout << "Unable to open file";
    }

    return 0;
}

